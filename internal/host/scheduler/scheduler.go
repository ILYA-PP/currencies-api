package scheduler

import (
	"currencies-api/apps/host/config"
	"currencies-api/internal/host/businesslayer/domain/currency"
	"currencies-api/internal/host/datalayer/storage/cache"
	"currencies-api/pkg/currencyclient"
	clmodels "currencies-api/pkg/currencyclient/models"
	"currencies-api/pkg/retry"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/rs/zerolog"
)

func Setup(config config.AppConfig, logger zerolog.Logger) {
	currencyProc := currency.NewCurrencyProcessor(
		currencyclient.NewCbrClient(config.CurrencyClient, logger),
		cache.NewCurrencyRepository(),
		retry.NewRetrier[*clmodels.ValCurs](config.RetryPolicy, logger),
		logger,
	)
	scheduler := gocron.NewScheduler(time.Local)
	scheduler.Every(1).Day().At(config.Server.Scheduler).Do(currencyProc.GetCurrencies)

	logger.Info().Msg("start schedulling every day at " + config.Server.Scheduler)

	scheduler.StartBlocking()
}
