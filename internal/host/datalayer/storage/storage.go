package storage

import "currencies-api/internal/host/datalayer/models"

type CurrencyRepository interface {
	InsertBulk(date string, currencies []models.Currency) error
	GetByDate(date string) ([]models.Currency, error)
}
