package cache

import (
	"currencies-api/internal/host/datalayer/models"
	"sync"
)

var cache = map[string][]models.Currency{}

type CurrencyRepository struct {
	mux sync.Mutex
}

func NewCurrencyRepository() *CurrencyRepository {
	return new(CurrencyRepository)
}

func (cr *CurrencyRepository) InsertBulk(date string, currencies []models.Currency) error {
	cr.mux.Lock()
	defer cr.mux.Unlock()

	cache[date] = currencies

	return nil
}

func (cr *CurrencyRepository) GetByDate(date string) ([]models.Currency, error) {
	cr.mux.Lock()
	defer cr.mux.Unlock()

	return cache[date], nil
}
