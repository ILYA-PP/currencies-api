package controllers

import (
	"currencies-api/internal/host/businesslayer"
	"encoding/json"
	"net/http"
)

type CurrencyController struct {
	currencyProcessor businesslayer.CurrecyProcessor
}

func NewCurrencyController(currencyProcessor businesslayer.CurrecyProcessor) *CurrencyController {
	return &CurrencyController{
		currencyProcessor: currencyProcessor,
	}
}

func (c *CurrencyController) GetCurrencies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	result := c.currencyProcessor.GetCurrencies()

	w.WriteHeader(result.StatusCode)
	json.NewEncoder(w).Encode(result.Data)
}
