package models

type Response struct {
	StatusCode int
	Data       interface{}
}

type MessageResponse struct {
	Message string `json:"message"`
}
