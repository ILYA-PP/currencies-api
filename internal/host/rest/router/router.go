package router

import (
	"currencies-api/apps/host/config"
	"currencies-api/internal/host/businesslayer/domain/currency"
	"currencies-api/internal/host/datalayer/storage/cache"
	"currencies-api/internal/host/rest/controllers"
	"currencies-api/internal/host/rest/middlewares"
	"currencies-api/pkg/currencyclient"
	clmodels "currencies-api/pkg/currencyclient/models"
	"currencies-api/pkg/retry"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
)

func Setup(config config.AppConfig, logger zerolog.Logger) *mux.Router {
	router := mux.NewRouter()

	router.Use(middlewares.GlobalErrorHandler)

	currencyCtrl := controllers.NewCurrencyController(
		currency.NewCurrencyProcessor(
			currencyclient.NewCbrClient(config.CurrencyClient, logger),
			cache.NewCurrencyRepository(),
			retry.NewRetrier[*clmodels.ValCurs](config.RetryPolicy, logger),
			logger,
		),
	)

	router.HandleFunc("/currencies/actual", currencyCtrl.GetCurrencies).Methods("GET")

	return router
}
