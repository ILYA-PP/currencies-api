package api

import (
	"currencies-api/apps/host/config"
	"currencies-api/internal/host/rest/router"
	"net/http"

	"github.com/rs/zerolog"
)

func Run(config config.AppConfig, logger zerolog.Logger) {
	router := router.Setup(config, logger)
	logger.Info().Msg("Running on port: " + config.Server.Port)

	http.ListenAndServe(":"+config.Server.Port, router)
}
