package currency

import (
	dlmodels "currencies-api/internal/host/datalayer/models"
	"currencies-api/internal/host/datalayer/storage"
	"currencies-api/internal/host/rest/models"
	"currencies-api/pkg/currencyclient"
	clmodels "currencies-api/pkg/currencyclient/models"
	"currencies-api/pkg/retry"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog"
)

type CurrencyProcessor struct {
	currencyClient     currencyclient.CurrencyClient
	currencyRepository storage.CurrencyRepository
	retrier            retry.Retrier[*clmodels.ValCurs]
	logger             zerolog.Logger
}

func NewCurrencyProcessor(
	currencyClient currencyclient.CurrencyClient,
	currencyRepository storage.CurrencyRepository,
	retrier retry.Retrier[*clmodels.ValCurs],
	logger zerolog.Logger,
) *CurrencyProcessor {
	return &CurrencyProcessor{
		currencyClient:     currencyClient,
		currencyRepository: currencyRepository,
		retrier:            retrier,
		logger:             logger,
	}
}

func (p *CurrencyProcessor) GetCurrencies() *models.Response {
	date := time.Now().Format("2006-01-02")
	storedCurrencies, err := p.currencyRepository.GetByDate(date)
	if err != nil {
		p.logger.Error().Err(err).Msg("failed to get currencies from storage")
		return &models.Response{
			StatusCode: 500,
			Data:       nil,
		}
	}

	if len(storedCurrencies) != 0 {
		return &models.Response{
			StatusCode: 200,
			Data:       storedCurrencies,
		}
	}

	currencies, err := p.retrier.Run(p.currencyClient.GetCurrencies)
	if err != nil {
		p.logger.Error().Err(err).Msg("failed to get currencies from cbr")
		return &models.Response{
			StatusCode: 500,
			Data:       nil,
		}
	}

	currenciesDB := make([]dlmodels.Currency, 0, len(currencies.Valutes))
	for _, currency := range currencies.Valutes {
		currencyDB, err := p.mapToDB(currency)
		if err != nil {
			return &models.Response{
				StatusCode: 500,
				Data:       nil,
			}
		}

		currenciesDB = append(currenciesDB, currencyDB)
	}

	err = p.currencyRepository.InsertBulk(date, currenciesDB)
	if err != nil {
		p.logger.Error().Err(err).Msg("failed to insert currencies")
		return &models.Response{
			StatusCode: 500,
			Data:       nil,
		}
	}

	return &models.Response{
		StatusCode: 200,
		Data:       currenciesDB,
	}
}

func (p CurrencyProcessor) mapToDB(currency clmodels.Valute) (dlmodels.Currency, error) {
	var parseErr error
	nominal, err := strconv.Atoi(currency.Nominal)
	if err != nil {
		p.logger.Error().Err(err).Msg("failed to convert currency.Nominal")
		parseErr = err
	}

	value, err := strconv.ParseFloat(strings.Replace(currency.Value, ",", ".", -1), 64)
	if err != nil {
		p.logger.Error().Err(err).Msg("failed to convert currency.Value")
		parseErr = err
	}

	vunitRate, err := strconv.ParseFloat(strings.Replace(currency.VunitRate, ",", ".", -1), 64)
	if err != nil {
		p.logger.Error().Err(err).Msg("failed to convert currency.VunitRate")
		parseErr = err
	}

	currenciesDB := dlmodels.Currency{
		NumCode:   currency.NumCode,
		CharCode:  currency.CharCode,
		Nominal:   nominal,
		Name:      currency.Name,
		Value:     value,
		VunitRate: vunitRate,
	}

	return currenciesDB, parseErr
}
