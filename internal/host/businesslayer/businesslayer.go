package businesslayer

import (
	"currencies-api/internal/host/rest/models"
)

type CurrecyProcessor interface {
	GetCurrencies() *models.Response
}
