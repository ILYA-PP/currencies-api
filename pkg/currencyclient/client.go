package currencyclient

import "currencies-api/pkg/currencyclient/models"

type CurrencyClient interface {
	GetCurrencies() (*models.ValCurs, error)
}
