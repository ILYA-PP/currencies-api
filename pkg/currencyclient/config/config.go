package config

type CurrencyClientConfig struct {
	DailyUrl string `mapstructure:"daily_url"`
}
