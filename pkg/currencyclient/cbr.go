package currencyclient

import (
	"currencies-api/pkg/currencyclient/config"
	"currencies-api/pkg/currencyclient/models"
	"currencies-api/pkg/xml"
	"errors"
	"fmt"
	"io"

	"github.com/rs/zerolog"

	"net/http"
)

type CbrClient struct {
	config config.CurrencyClientConfig
	client *http.Client
	logger zerolog.Logger
}

func NewCbrClient(config config.CurrencyClientConfig, logger zerolog.Logger) *CbrClient {
	return &CbrClient{
		config: config,
		client: &http.Client{},
		logger: logger,
	}
}

func (c *CbrClient) GetCurrencies() (*models.ValCurs, error) {
	method := http.MethodGet
	url := c.config.DailyUrl

	req, err := http.NewRequest(method, url, nil)
	req.Header.Add("User-Agent", `currencies-api`)
	if err != nil {
		c.logger.Error().Err(err).Str("method", method).Str("url", url).Msg("failed to create get currencies request")
		return nil, err
	}

	resp, err := c.client.Do(req)
	if err != nil {
		c.logger.Error().Err(err).Str("method", method).Str("url", url).Msg("failed to send get currencies request")
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		c.logger.Error().
			Str("method", method).
			Str("url", url).
			Str("code", fmt.Sprint(resp.StatusCode)).
			Msg("ivalid response status code")
		return nil, errors.New("bad status code")
	}

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		c.logger.Error().Err(err).Str("method", method).Str("url", url).Msg("failed to read get currencies response")
		return nil, err
	}

	currensies, err := xml.Decode[models.ValCurs](respBytes)
	if err != nil {
		c.logger.Error().Err(err).Str("method", method).Str("url", url).Msg("failed to deserialize get currencies response")
		return nil, err
	}

	return currensies, nil
}
