package xml

import (
	"bytes"
	"encoding/xml"

	"golang.org/x/net/html/charset"
)

func Decode[T any](xmlData []byte) (*T, error) {
	res := new(T)
	reader := bytes.NewReader(xmlData)
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = charset.NewReaderLabel
	err := decoder.Decode(&res)

	return res, err
}
