package config

type RetryPolicyConfig struct {
	RetryCount int `mapstructure:"retry_count"`
}
