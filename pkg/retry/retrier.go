package retry

import (
	"currencies-api/pkg/retry/config"

	"github.com/rs/zerolog"
)

type Retrier[T any] struct {
	config config.RetryPolicyConfig
	logger zerolog.Logger
}

func NewRetrier[T any](config config.RetryPolicyConfig, logger zerolog.Logger) Retrier[T] {
	return Retrier[T]{
		config: config,
		logger: logger,
	}
}

func (r *Retrier[T]) Run(action func() (T, error)) (T, error) {
	var glErr error
	var glRes T

	for i := 1; i <= r.config.RetryCount; i++ {
		res, err := action()
		if err != nil {
			r.logger.Error().Err(err).Int("attempt", i).Msg("failed to runnig action")
			glErr = err
			continue
		}

		return res, nil
	}

	return glRes, glErr
}
