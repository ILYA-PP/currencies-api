package config

import (
	cur "currencies-api/pkg/currencyclient/config"
	retry "currencies-api/pkg/retry/config"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

type AppConfig struct {
	Server         ServerConfig
	CurrencyClient cur.CurrencyClientConfig `mapstructure:"currency_client"`
	RetryPolicy    retry.RetryPolicyConfig  `mapstructure:"retry_policy"`
}

type ServerConfig struct {
	Port      string
	Mode      string
	Scheduler string
}

func Setup(logger zerolog.Logger) (AppConfig, error) {
	var config AppConfig

	viper.AddConfigPath("etc")
	viper.SetConfigName("host.config.yml")
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		logger.Fatal().Err(err).Msg("failed to read config file")
		return config, err
	}

	err := viper.Unmarshal(&config)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to desialize config file")
		return config, err
	}

	return config, err
}
