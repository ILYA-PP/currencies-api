package main

import (
	"currencies-api/apps/host/config"
	api "currencies-api/internal/host/rest"
	"currencies-api/internal/host/scheduler"
	"os"

	"github.com/rs/zerolog"
)

func main() {
	logger := zerolog.New(os.Stdout).With().Timestamp().Logger()

	config, err := config.Setup(logger)
	if err != nil {
		return
	}

	go scheduler.Setup(config, logger)
	api.Run(config, logger)
}
